// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function postRequest(url: string, payload: any) {
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(payload)
  })

  const responseContent = await response.json()

  if (response.ok) {
    return responseContent
  } else {
    throw new Error(JSON.stringify(responseContent))
  }
}
