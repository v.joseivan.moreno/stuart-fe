import Geocoding from 'features/geocoding/ui/Geocoding'
import JobSuccessNotification from 'features/jobs/ui/JobSuccessNotification'
import styled from 'styled-components'
import { useEffect, useMemo, useState } from 'react'
import { useAppSelector } from './state'
import { GeocodedAddress } from 'features/geocoding/types'
import pickUpMarkerIcon from 'assets/pickUpMarker.svg'
import dropOffMarkerIcon from 'assets/dropOffMarker.svg'
import useGoogleMapWithScript from 'features/map/ui/GoogleMapWithScript'
import { useCallback } from 'react'


const Map = styled.div`
  width: 100vw;
  height: 100vh;
`

function App() {
  const dropOffAddress: GeocodedAddress = useAppSelector(state => state.geocoding.dropOffAddress.value)
  const pickUpAddress: GeocodedAddress = useAppSelector(state => state.geocoding.pickUpAddress.value)
  const [dropOffMarker, setDropOffMarker] = useState<unknown>()
  const [pickUpMarker, setPickUpMarker] = useState<unknown>()

  const gMapsOptions = useMemo(() => ({
    disableDefaultUI: true,
    center: { lat: 48.8566, lng: 2.35 },
    zoom: 12
  }), [])

  // const { isReady, initializeMarker, placeMarker, removeMarker } = useGoogleMap({ selector: '#map', options: gMapsOptions })
  const { isReady, initializeMarker, placeMarker, removeMarker } = useGoogleMapWithScript({ selector: '#map', options: gMapsOptions })
  const geocodedAddresToLatLng = useCallback((geocodedAddress: GeocodedAddress) => {
    return {
      lat: geocodedAddress.latitude,
      lng: geocodedAddress.longitude,
    }
  }, [])

  useEffect(() => {
    if (isReady) {
      setPickUpMarker(initializeMarker(pickUpMarkerIcon))
      setDropOffMarker(initializeMarker(dropOffMarkerIcon))
    }
  }, [isReady, initializeMarker])

  useEffect(() => {
    if (!pickUpMarker) return
    if (pickUpAddress) {
      const position = geocodedAddresToLatLng(pickUpAddress)
      placeMarker(pickUpMarker, { position })
    } else {
      removeMarker(pickUpMarker)
    }
  }, [pickUpAddress, pickUpMarker, placeMarker, removeMarker, geocodedAddresToLatLng])
  
  useEffect(() => {
    if (!dropOffMarker) return
    if (dropOffAddress) {
      const position = geocodedAddresToLatLng(dropOffAddress)
      placeMarker(dropOffMarker, { position })
    } else {
      removeMarker(dropOffMarker)
    }
  }, [dropOffAddress, dropOffMarker, placeMarker, removeMarker, geocodedAddresToLatLng])


  return (
    <div className="App">
      <Geocoding />
      <Map id="map" />
      <JobSuccessNotification />
    </div>
  )
}

export default App
