import { configureStore, Store } from '@reduxjs/toolkit'
import createSagaMiddleware from 'redux-saga'
import reducer from './reducer'
import rootSaga from './saga'

let store: Store

export function createAppStore(preloadedState = {}) {
  const sagaMiddleware = createSagaMiddleware()
  const middleware = [sagaMiddleware]

  let canceled = false
  const store = configureStore({
    reducer,
    middleware,
    preloadedState
  })

  const sagasTask = sagaMiddleware.run(rootSaga)

  return {
    store,
    sagaMiddleware,
    destroy: async () => {
      if (!canceled) {
        sagasTask.cancel()
        canceled = true
      }
      await sagasTask.toPromise()
    }
  }
}

const registerStore = (newStore: Store) => {
  store = newStore
}

export const appStore = createAppStore()

registerStore(appStore.store)

export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof store.getState>
