import geocodingSaga from 'features/geocoding/state/geocodingSaga'
import jobsSaga from 'features/jobs/state/jobSaga'
import { spawn } from 'redux-saga/effects'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function* spawnSaga(generator: any) {
  try {
    yield spawn(generator)
  } catch (e) {
    console.error('Error handling spawn Saga:', e)
  }
}

export default function* rootSaga() {
  yield spawnSaga(geocodingSaga)
  yield spawnSaga(jobsSaga)
}
