import geocodingReducer from 'features/geocoding/state/geocodingSlice'
import jobsReducer from 'features/jobs/state/jobSlice'

const reducer = {
  geocoding: geocodingReducer,
  jobs: jobsReducer
}

export default reducer
