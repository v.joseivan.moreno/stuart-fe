import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux'
import type { RootState, AppDispatch } from './store'

export enum RequestStatus {
  Success = 'success',
  Rejected = 'rejected',
  Pending = 'pending',
  Idle = 'idle'
}

export interface StateBundle<T> {
  value?: T
  status: RequestStatus
}

export const useAppDispatch = () => useDispatch<AppDispatch>()
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector
