import { PayloadAction } from '@reduxjs/toolkit'
import { RequestStatus } from 'app/state'
import { setDropOffAddress, setPickUpAddress } from 'features/geocoding/state/geocodingSlice'
import { call, put, takeLatest } from 'redux-saga/effects'
import { createJob as createJobRequest } from '../api/jobsApi'
import { Job } from '../types'
import { fetchCreateJob, setJobStatus } from './jobSlice'

function* createJob(action: PayloadAction<Job>) {
  if (!action.payload) return

  try {
    yield put(setJobStatus(RequestStatus.Pending))
    yield call(createJobRequest, action.payload)
    yield put(setJobStatus(RequestStatus.Success))
    yield put(setJobStatus(RequestStatus.Idle))
    yield put(setPickUpAddress({ status: RequestStatus.Pending }))
    yield put(setDropOffAddress({ status: RequestStatus.Pending }))
  } catch (e) {
    console.error('Cannot create job', e)
    yield put(setJobStatus(RequestStatus.Rejected))
  }
}

function* watchJobsActions() {
  try {
    yield takeLatest(fetchCreateJob.type, createJob)
  } catch (e) {
    console.error('An error has occurred watching actions', e)
  }
}

export default function* jobsSaga() {
  try {
    yield watchJobsActions()
  } catch (e) {
    console.error('An error has occurred setting up JobSaga:', e)
  }
}
