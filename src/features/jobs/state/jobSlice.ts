import { createAction, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RequestStatus } from 'app/state'
import { Job } from '../types'

interface JobState {
  jobStatus: RequestStatus
}

const initialState: JobState = {
  jobStatus: RequestStatus.Idle
}

const name = 'jobs'
const FETCH_CREATE_JOB = `${name}/fetch_create_job`

const asyncActions = {
  fetchCreateJob: createAction<Job>(FETCH_CREATE_JOB)
}

const reducers = {
  setJobStatus: (state: JobState, action: PayloadAction<RequestStatus>) => {
    state.jobStatus = action.payload
  }
}

const GeocodingSlice = createSlice({
  name,
  initialState,
  reducers
})

export const { setJobStatus } = GeocodingSlice.actions

export const { fetchCreateJob } = asyncActions

export default GeocodingSlice.reducer
