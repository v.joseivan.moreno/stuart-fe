import { RequestStatus, useAppSelector } from 'app/state'
import { useCallback } from 'react'
import { useState } from 'react'
import { useEffect } from 'react'
import styled from 'styled-components'

const Notification = styled.div`
  position: absolute;
  display: flex;
  align-items: center;
  right: 32px;
  top: 32px;
  width: auto;
  height: 40px;
  padding: 0 8px;
  border-radius: 8px;
  background: rgba(51, 51, 51, 0.9);
  color: #fff;
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.1), 0 1px 8px 0 rgba(0, 0, 0, 0.1);
  pointer-events: none;
`

function JobSuccessNotification() {
  const jobStatus = useAppSelector((state) => state.jobs.jobStatus)
  const [displayNotification, setDisplayNotification] = useState(false)

  const showNotificationDuring = useCallback((time: number) => {
    setDisplayNotification(true)
    setTimeout(() => {
      setDisplayNotification(false)
    }, time)
  }, [])

  useEffect(() => {
    if (jobStatus === RequestStatus.Success) {
      showNotificationDuring(5000)
    }
  }, [jobStatus, showNotificationDuring])

  return <>{displayNotification && <Notification>Job has been created successfully!</Notification>}</>
}

export default JobSuccessNotification
