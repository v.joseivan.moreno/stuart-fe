import { postRequest } from 'app/api/api'
import { Job } from '../types'

export async function createJob(job: Job): Promise<void> {
  const url = 'https://stuart-frontend-challenge.vercel.app/jobs'

  await postRequest(url, job)
}
