export interface GeocodedAddress {
  address: string
  latitude: number
  longitude: number
}
