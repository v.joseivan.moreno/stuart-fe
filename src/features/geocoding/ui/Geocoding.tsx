import styled from 'styled-components'
import PickUp from './PickUp'
import DropOff from './DropOff'
import { GeocodedAddress } from '../types'
import { RequestStatus, useAppDispatch, useAppSelector } from 'app/state'
import { fetchCreateJob } from 'features/jobs/state/jobSlice'

const Box = styled.div`
  position: absolute;
  z-index: 9;
  top: 0px;
  left: 0px;
  width: 340px;
  padding: 16px;
  margin: 32px;
  background: #fff;
  border-radius: 8px;
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.1), 0 1px 8px 0 rgba(0, 0, 0, 0.1);
  display: flex;
  flex-flow: column;
`

const Button = styled.button`
  height: 40px;
  width: calc(100% - 36px);
  border-radius: 4px;
  color: #fff;
  box-shadow: 0 1px 2px 0 rgba(16, 162, 234, 0.3);
  background: linear-gradient(180deg, #10a2ea, #0f99e8);
  cursor: pointer;
  border: none;
  align-self: end;

  &:hover {
    background: linear-gradient(180deg, #10a2ea, #098bd5);
  }

  &:disabled {
    opacity: 0.5;
  }
`

function Geocoding() {
  const dispatch = useAppDispatch()
  const pickUpAddress: GeocodedAddress = useAppSelector(state => state.geocoding.pickUpAddress.value)
  const dropOffAddress: GeocodedAddress = useAppSelector(state => state.geocoding.dropOffAddress.value)
  const jobStatus: RequestStatus = useAppSelector(state => state.jobs.jobStatus)
  const isCreatingJob = jobStatus === RequestStatus.Pending

  const shouldBeDisabled = !(pickUpAddress && dropOffAddress) || isCreatingJob

  const handleSubmit = () => {
    dispatch(fetchCreateJob({ dropoff: dropOffAddress.address, pickup: pickUpAddress.address }))
  }

  return (
    <Box>
      <PickUp />
      <DropOff />
      <Button onClick={handleSubmit} disabled={shouldBeDisabled}>
        {isCreatingJob ? 'Creating...' : 'Create job'}
      </Button>
    </Box>
  )
}

export default Geocoding
