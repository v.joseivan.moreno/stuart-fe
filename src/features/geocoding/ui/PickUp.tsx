import GeocodingEntry from './GeocodingEntry/GeocodingEntry'
import { ReactComponent as PickUpIcon } from 'assets/pickUpBadgeBlank.svg'
import { ReactComponent as PickUpSuccessIcon } from 'assets/pickUpBadgePresent.svg'
import { ReactComponent as PickUpErrorIcon } from 'assets/pickUpBadgeError.svg'
import { useDispatch } from 'react-redux'
import { fetchGeocodedPickupAddress } from '../state/geocodingSlice'
import { useEffect, useMemo, useState } from 'react'
import { RequestStatus, useAppSelector } from 'app/state'

function PickUp() {
  const dispatch = useDispatch()
  const [address, setAddress] = useState<string | undefined>('') 
  const pickUpStatus = useAppSelector(state => state.geocoding.pickUpAddress.status)
  const jobStatus = useAppSelector((state) => state.jobs.jobStatus)

  const handleOnChange = (address?: string) => {
    setAddress(address)
    dispatch(fetchGeocodedPickupAddress(address))
  }

  const icon = useMemo(() => {
    if (pickUpStatus === RequestStatus.Success) {
      return <PickUpSuccessIcon />
    } else if (pickUpStatus === RequestStatus.Rejected) {
      return <PickUpErrorIcon />
    } else {
      return <PickUpIcon />
    }
  }, [pickUpStatus])

  useEffect(() => {
    if (jobStatus === RequestStatus.Idle) setAddress('')
  }, [jobStatus])

  return <GeocodingEntry placeholder={'Pick up address'} value={address} onChange={handleOnChange} icon={icon} />
}

export default PickUp
