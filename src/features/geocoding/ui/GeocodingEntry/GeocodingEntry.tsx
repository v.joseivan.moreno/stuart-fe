import styled from 'styled-components'
import { ReactElement } from 'react'

interface GeocodingEntryProps {
  placeholder: string
  value?: string
  icon: ReactElement
  onChange: (address?: string) => void
}

const Entry = styled.div`
  display: flex;
  margin-bottom: 16px;
`

const Input = styled.input`
  height: 32px;
  padding-left: 8px;
  margin-left: 8px;
  border-radius: 4px;
  background: #f0f3f7;
  width: 100%;
  outline: none;
  border: none;

  &::placeholder {
    color: #8596a6;
  }
`

function GeocodingEntry({ placeholder, value, icon, onChange }: GeocodingEntryProps) {
  /*
    Here I have two options, use a debounce in the onchange event or delegate it to Saga. 
    In this case, I prefer to delegate to Saga because the only thing that I'm going to do is send a request to the API
  */

  return (
    <Entry>
      {icon}
      <Input placeholder={placeholder} value={value} onChange={(e) => onChange(e.currentTarget.value)} />
    </Entry>
  )
}

export default GeocodingEntry
