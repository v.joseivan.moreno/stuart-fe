import GeocodingEntry from './GeocodingEntry/GeocodingEntry'
import { ReactComponent as DropOffIcon } from 'assets/dropOffBadgeBlank.svg'
import { ReactComponent as DropOffSuccessIcon } from 'assets/dropOffBadgePresent.svg'
import { ReactComponent as DropOffErrorIcon } from 'assets/dropOffBadgeError.svg'
import { RequestStatus, useAppDispatch, useAppSelector } from 'app/state'
import { fetchGeocodedDropOffAddress } from '../state/geocodingSlice'
import { useMemo, useState } from 'react'
import { useEffect } from 'react'

function DropOff() {
  const dispatch = useAppDispatch()
  const [address, setAddress] = useState<string | undefined>('') 
  const dropOffStatus = useAppSelector((state) => state.geocoding.dropOffAddress.status)
  const jobStatus = useAppSelector((state) => state.jobs.jobStatus)

  const handleOnChange = (address?: string) => {
    setAddress(address)
    dispatch(fetchGeocodedDropOffAddress(address))
  }

  const icon = useMemo(() => {
    if (dropOffStatus === RequestStatus.Success) {
      return <DropOffSuccessIcon />
    } else if (dropOffStatus === RequestStatus.Rejected) {
      return <DropOffErrorIcon />
    } else {
      return <DropOffIcon />
    }
  }, [dropOffStatus])

  useEffect(() => {
    if (jobStatus === RequestStatus.Idle) setAddress('')
  }, [jobStatus])

  return <GeocodingEntry placeholder={'Drop off address'} value={address} onChange={handleOnChange} icon={icon} />
}

export default DropOff
