import { postRequest } from 'app/api/api'
import { GeocodedAddress } from '../types'

export async function geocodeAddress(address: string): Promise<GeocodedAddress> {
  const url = 'https://stuart-frontend-challenge.vercel.app/geocode'

  const response = await postRequest(url, { address })
  return response
}
