import { createAction, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RequestStatus, StateBundle } from 'app/state'
import { GeocodedAddress } from '../types'

interface GeocodingState {
  pickUpAddress: StateBundle<GeocodedAddress>
  dropOffAddress: StateBundle<GeocodedAddress>
}

const initialState: GeocodingState = {
  pickUpAddress: {
    status: RequestStatus.Idle
  },
  dropOffAddress: {
    status: RequestStatus.Idle
  }
}

const name = 'geocoding'
const FETCH_GEOCODED_PICKUP_ADDRESS = `${name}/fetch_geocoded_pickup_address`
const FETCH_GEOCODED_DROPOFF_ADDRESS = `${name}/fetch_geocoded_dropoff_address`

const asyncActions = {
  fetchGeocodedPickupAddress: createAction<string | undefined>(FETCH_GEOCODED_PICKUP_ADDRESS),
  fetchGeocodedDropOffAddress: createAction<string | undefined>(FETCH_GEOCODED_DROPOFF_ADDRESS),
}

const reducers = {
  setPickUpAddress: (state: GeocodingState, action: PayloadAction<StateBundle<GeocodedAddress>>) => {
    state.pickUpAddress = action.payload
  },
  setDropOffAddress: (state: GeocodingState, action: PayloadAction<StateBundle<GeocodedAddress>>) => {
    state.dropOffAddress = action.payload
  }
}

const GeocodingSlice = createSlice({
  name,
  initialState,
  reducers
})

export const { setPickUpAddress, setDropOffAddress } = GeocodingSlice.actions

export const { fetchGeocodedPickupAddress, fetchGeocodedDropOffAddress } = asyncActions

export default GeocodingSlice.reducer
