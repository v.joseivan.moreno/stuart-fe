import { PayloadAction } from '@reduxjs/toolkit'
import { RequestStatus } from 'app/state'
import { call, debounce, put } from 'redux-saga/effects'
import { geocodeAddress } from '../api/geocodingApi'
import { GeocodedAddress } from '../types'
import {
  fetchGeocodedPickupAddress,
  setPickUpAddress,
  setDropOffAddress,
  fetchGeocodedDropOffAddress,
} from './geocodingSlice'

function* geocodePickUpAddress(action: PayloadAction<string>) {
  if (!action.payload) return

  try {
    yield put(setPickUpAddress({ status: RequestStatus.Pending }))
    const geocodedAddress: GeocodedAddress = yield call(geocodeAddress, action.payload)
    yield put(setPickUpAddress({ value: geocodedAddress, status: RequestStatus.Success }))
  } catch (e) {
    console.error('Cannot geocode address', e)
    yield put(setPickUpAddress({ status: RequestStatus.Rejected }))
  }
}

function* geocodeDropOffAddress(action: PayloadAction<string>) {
  if (!action.payload) return

  try {
    yield put(setDropOffAddress({ status: RequestStatus.Pending }))
    const geocodedAddress: GeocodedAddress = yield call(geocodeAddress, action.payload)
    yield put(setDropOffAddress({ value: geocodedAddress, status: RequestStatus.Success }))
  } catch (e) {
    console.error('Cannot geocode address', e)
    yield put(setDropOffAddress({ status: RequestStatus.Rejected }))
  }
}

function* watchGeocodingActions() {
  try {
    yield debounce(600, fetchGeocodedPickupAddress.type, geocodePickUpAddress)
    yield debounce(600, fetchGeocodedDropOffAddress.type, geocodeDropOffAddress)
  } catch (e) {
    console.error('An error has occurred watching actions', e)
  }
}

export default function* geocodingSaga() {
  try {
    yield watchGeocodingActions()
  } catch (e) {
    console.error('An error has occurred setting up GeocodingSaga:', e)
  }
}
