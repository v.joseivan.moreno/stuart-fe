/* eslint-disable @typescript-eslint/no-explicit-any */
import { useCallback, useEffect, useState } from 'react'

interface UseGoogleMapWithScriptProps {
  selector: string
  options: {
    center: { lat: number; lng: number }
    zoom: number
  }
}
function useGoogleMapWithScript({ selector, options }: UseGoogleMapWithScriptProps) {
  
  const apiKey = 'AIzaSyBRy8ccW2X0p_vVf_5eYD0NiUt6wtm5nog'
  const [loaded, setLoaded] = useState(false)
  const [google, setGoogle] = useState<any>()
  const [map, setMap] = useState<any>()
  const isReady = google && map

  useEffect(() => {
    const loadGoogleMap = async () => {
      setLoaded(true)
      const script = document.createElement('script')
      script.src = `https://maps.googleapis.com/maps/api/js?key=${apiKey}`
      script.onload = () => {
        setGoogle(window.google)
      }
      document.head.appendChild(script);
    }
    if (!loaded) loadGoogleMap()
  }, [google, loaded, setGoogle, setLoaded])

  useEffect(() => {
    if (google) {
      const map = new google.maps.Map(document.querySelector(selector), options)

      setMap(map)
    }
  }, [google, selector, options, setMap])

  const checkIfIsReady = useCallback(() => {
    if (isReady) return
    if (!google) throw new Error('google is not loaded yet')
    if (!map) throw new Error('map is not loaded yet')
  }, [google, map, isReady])

  const initializeMarker = useCallback(
    (icon?) => {
      checkIfIsReady()
      const marker = new google.maps.Marker({
        ...(icon
          ? {
              icon: {
                url: icon
              }
            }
          : {})
      })
      return marker
    },
    [google, checkIfIsReady]
  )

  const placeMarker = useCallback(
    (marker, { position }) => {
      checkIfIsReady()
      marker.setPosition(position)
      marker.setMap(map)

      return marker
    },
    [map, checkIfIsReady]
  )

  const removeMarker = useCallback(
    (marker) => {
      checkIfIsReady()
      marker.setMap(null)

      return marker
    },
    [checkIfIsReady]
  )

  return { map, google, isReady, initializeMarker, placeMarker, removeMarker }
}

export default useGoogleMapWithScript
