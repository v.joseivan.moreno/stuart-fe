import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './app/App'
import { Provider } from 'react-redux'
import { appStore } from 'app/state/store'

ReactDOM.render(
  <React.StrictMode>
    <Provider store={appStore.store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
)
