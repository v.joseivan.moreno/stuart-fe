/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable no-undef */

module.exports = {
  plugins: ['react-redux', 'react-hooks', 'redux-saga'],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react-redux/recommended',
    'plugin:react-hooks/recommended',
    'plugin:redux-saga/recommended',
    'eslint-config-prettier'
  ],
  rules: {
    '@typescript-eslint/no-unused-vars': 'warn'
  },
  settings: {
    react: {
      version: 'detect'
    }
  }
}
