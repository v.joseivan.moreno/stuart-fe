### About the project
I've started this project using CRA, not the typescript template. All the "tooling" has been added manually (Typescript, prettier, lint, redux, etc.). It took me about 6~7 hours to complete this challenge.


### Why have I not made any changes to Webpack or Babel?
I've not done anything about Webpack and Babel because I'm using CRA. I am aware that I have several ways to configure both and I have experience working with CRACO, for example. But in this particular case I did not see the need of making any changes to any of them.

### How is GoogleMaps being loaded?
Reading the requirements for the challenge it wasn't clear to me if I could use a library to load GMaps, so I decided to include both examples: one using a very simple GMaps library (not official, first result searching in Google) and one importing the library dinamically.
Both are available via hooks in the project:
1. GoogleMap.tsx
2. GoogleMapWithScript.tsx (by default)

### Improvements
* Add any theming library, such as Material UI or others.
* Improve typings for Redux-Saga and GMaps.
* Add unit tests for the Geocoding component.
* Show a notification also when the create job fails.
* Give better feedback to the user when the address cannot be geocoded.
* User environment files to define the GMaps apiKey



